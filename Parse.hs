module Parse where

import Control.Applicative
import Control.Monad
import Data.Monoid

newtype Parse e s a = Parse { runParse :: s -> Either e (a, s) }

instance Monad (Parse e s) where
    return x = Parse $ \s -> Right (x, s)
    (Parse m) >>= f =
        Parse $ \s -> case m s of
            Left      e   -> Left e
            Right (a, s') -> runParse (f a) s'

instance Functor (Parse e s) where
    fmap = liftM

instance Applicative (Parse e s) where
    pure = return
    (<*>) = ap

instance (Monoid e) => Alternative (Parse e s) where
    empty = Parse $ \s -> Left mempty
    p1 <|> p2 =
        Parse $ \s -> case runParse p1 s of
            Left e1 -> case runParse p2 s of
                Left  e2 -> Left $ e1 <> e2
                Right y  -> Right y
            Right x -> Right x

choice :: (Monoid e) => [Parse e s a] -> Parse e s a
choice = foldr (<|>) empty

type Parser = Parse String String

char :: Char -> Parser Char
char c' = Parse $ \s -> case s of
    []   -> err
    c:cs -> if (c == c') then Right (c, cs) else err
  where err = Left $ c':" "

anyChar :: Parser Char
anyChar = Parse $ \s -> case s of
    []   -> Left "anyChar"
    c:cs -> Right (c, cs)

eof :: Parser ()
eof = Parse $ \s -> case s of
    []   -> Right ((), "")
    "\n" -> Right ((), "")
    _  -> Left "eof"