module Base where

import Control.Applicative
import Control.Monad
import Data.Char
import Parse
import System.Environment (getArgs)


data Operator = 
  Plus
  | Minus
  | RShift
  | LShift
  | Dot
  | Comma
  | Bracket STree
  deriving (Show)
           
type STree = [Operator]

-- Tape manipulation

type Tape = ([Int], Int, [Int])

makeChars :: Tape
makeChars = (repeat 0, 0, repeat 0)

lshift :: Tape -> Tape
lshift (l:ls, c, r) = (ls, l, c:r)

rshift :: Tape -> Tape
rshift (l, c, r:rs) = (c:l, r, rs)

app :: (Int -> Int) -> Tape -> Tape
app f (l, c, r) = (l, f c, r)

incr :: Tape -> Tape
incr = app succ

decr :: Tape -> Tape
decr = app pred

-- Parsing

mappings :: [(Char, Operator)]
mappings = zip "+-><.," [Plus, Minus, RShift, LShift, Dot, Comma]

operator :: Parser Operator
operator = choice $ map (\(c, op) -> char c *> pure op) mappings

bracket :: Parser [Operator] -> Parser Operator
bracket p = do
    char '['
    ops <- p
    char ']'
    return $ Bracket ops

sTree :: Parser STree
sTree = many $ bracket sTree <|> operator

parse :: String -> Either String STree
parse = fmap fst . runParse (sTree <* eof)
