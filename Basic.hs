module Basic where

import Base
import Data.Functor
import Data.Char

type Runtime = (STree, Tape)

(<$$>) f (a, b) = (f a, b) -- First member manipulation

data StateLoop = InLoop | OutLoop

run :: StateLoop -> Runtime -> IO ()
run _ ([], _) = return ()
run stLoop runT@(op, (l, c, r)) = do
    case head op of
      Plus -> act incr
      Minus -> act decr
      RShift -> act rshift
      LShift -> act lshift
      Dot -> do
        putChar . chr $ c
        run stLoop $ treatment <$$> runT
      Comma -> do
        c <- getChar
        run stLoop $ (app . const . ord $ c) <$> treatment <$$> runT
      Bracket innerCode -> return ()
        
      where treatment = case stLoop of
              InLoop -> id
              OutLoop -> tail
            act f = run stLoop $ f <$> treatment <$$> runT
              
        
        
test = fromRight . parse $ "++++++++++[>+++++++>++++++++++>+++>+<<<<-]>++.>+.+++++++..+++.>++.<<+++++++++++++++.>.+++.------.--------.>+.>."

fromRight (Right x) = x